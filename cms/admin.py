from django.contrib import admin
from cms.models import Pagina

# Register your models here.

class PaginaAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('titulo',)}

admin.site.register(Pagina, PaginaAdmin)