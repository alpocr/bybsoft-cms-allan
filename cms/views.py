from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils import timezone

from cms.models import Pagina 

# Create your views here.

class PaginaListView(ListView):
    model = Pagina
    

class PaginaDetailView(DetailView):
    model = Pagina
    