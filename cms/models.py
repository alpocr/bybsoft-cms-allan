from django.db import models
from django.template.defaultfilters import slugify
from tinymce.models import HTMLField

# Create your models here.


class Pagina(models.Model):
    """
    Representa una pagina dentro del CMS
    """
    titulo = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, blank=True)
    cuerpo = HTMLField()