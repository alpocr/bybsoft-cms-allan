from django.conf.urls import patterns, include, url
from django.conf import settings

from cms.views import PaginaListView, PaginaDetailView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bybsoft.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', PaginaListView.as_view(), name='pagina-list'),
    url(r'^(?P<slug>[-_\w]+)/$', PaginaDetailView.as_view(), name='pagina-detail'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
